# SteamCMD Docker

This is a Docker image for running SteamCMD in an Ubuntu 18.04 environment.

It is non-interactive and can be called from scripts for automation.

### What does it do?

This image uses Ubuntu 18.04 as the base.

It can take in a Steam AppID, your steam username and password and will install the server in a directory that you choose.

### How to use it

Download the Dockerfile and build it (naming it `steamcmd`).

Then:

```
docker run \
-e APP_ID="<app_id>" \
-e USERNAME="<username>" \
-e PASSWORD="<password>" \
-v ~/<game server folder>:/server \
steamcmd
```

If you would like to login to steam anonymously simply leave out the `USERNAME` and `PASSWORD` fields.